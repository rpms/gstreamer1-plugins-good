From 8d4c79e61a62245dc6a499b0a439317bb37d0508 Mon Sep 17 00:00:00 2001
From: Wim Taymans <wtaymans@redhat.com>
Date: Mon, 16 Dec 2024 11:47:09 +0100
Subject: [PATCH 5/9] gdkpixbufdec: Check if initializing the video info
 actually succeeded

Otherwise a 0-byte buffer would be allocated, which gives NULL memory when
mapped.

Thanks to Antonio Morales for finding and reporting the issue.

Fixes GHSL-2024-118
Fixes https://gitlab.freedesktop.org/gstreamer/gstreamer/-/issues/3876

Part-of: <https://gitlab.freedesktop.org/gstreamer/gstreamer/-/merge_requests/8103>
---
 ext/gdk_pixbuf/gstgdkpixbufdec.c | 9 ++++++++-
 1 file changed, 8 insertions(+), 1 deletion(-)

diff --git a/ext/gdk_pixbuf/gstgdkpixbufdec.c b/ext/gdk_pixbuf/gstgdkpixbufdec.c
index c119236a8..c0ecb3a08 100644
--- a/ext/gdk_pixbuf/gstgdkpixbufdec.c
+++ b/ext/gdk_pixbuf/gstgdkpixbufdec.c
@@ -318,7 +318,8 @@ gst_gdk_pixbuf_dec_flush (GstGdkPixbufDec * filter)
 
 
     gst_video_info_init (&info);
-    gst_video_info_set_format (&info, fmt, width, height);
+    if (!gst_video_info_set_format (&info, fmt, width, height))
+      goto format_not_supported;
     info.fps_n = filter->in_fps_n;
     info.fps_d = filter->in_fps_d;
     caps = gst_video_info_to_caps (&info);
@@ -379,6 +380,12 @@ channels_not_supported:
         ("%d channels not supported", n_channels));
     return GST_FLOW_ERROR;
   }
+format_not_supported:
+  {
+    GST_ELEMENT_ERROR (filter, STREAM, DECODE, (NULL),
+        ("%d channels with %dx%d not supported", n_channels, width, height));
+    return GST_FLOW_ERROR;
+  }
 no_buffer:
   {
     GST_DEBUG ("Failed to create outbuffer - %s", gst_flow_get_name (ret));
-- 
2.47.0

